*******************************
*******************************
This source code is from
ASP.NET 3.5 For Dummies
By Ken Cox
ISBN: 978-0-470-19592-5
Copyright � 2008 Ken Cox
*******************************
*******************************

For questions about the book's 
code (including updates), 
purchasers should register
at this site:

http://www.kencox.ca/

For general questions about 
ASP.NET and programming, please 
visit:

http://forums.asp.net/

*******************************

This code is provided "AS IS" 
for the convenience of book 
purchasers. The code is made 
available with no warranties 
and no conferred rights.

Use this information at your 
own risk. Seek advice from
a competent professional
before trying these stunts.

No animal (especially Goldie!)
was harmed in the creation of
this code.


*******************************